// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package config

import (
	"os"

	kratos "github.com/ory/kratos-client-go"
	"github.com/sirupsen/logrus"

	"entanglement.garden/common/config"
)

// Config is all runtime-configurable options
type Config struct {
	Bind               string                     `json:"bind"`
	APIEndpoint        string                     `json:"api_endpoint"`
	KratosServer       kratos.ServerConfiguration `json:"kratos"`
	KratosAdminServer  kratos.ServerConfiguration `json:"kratos_admin"`
	KratosPublicDomain string                     `json:"kratos_public_domain"`
	Images             []BaseImage                `json:"images"`
	Services           []string                   `json:"services"`
	PluginCache        string                     `json:"plugin_cache"`
	PluginSocketPath   string                     `json:"plugin_socket_path"`
}

var (
	// C stores the actual configured values
	C = Config{
		Bind:        ":8085",
		APIEndpoint: "http://localhost:8080",
		Images: []BaseImage{
			{Name: "Debian Bullseye", URL: "https://cloud.debian.org/images/cloud/bookworm/daily/latest/debian-12-generic-amd64-daily.qcow2"},
			{Name: "Debian Bookworm (testing)", URL: "https://cloud.debian.org/images/cloud/bookworm/daily/latest/debian-12-generic-amd64-daily.qcow2"},
		},
		KratosServer:      kratos.ServerConfiguration{URL: "http://ory-kratos.entanglement-garden-iam.svc.cluster.local:4433"},
		KratosAdminServer: kratos.ServerConfiguration{URL: "http://ory-kratos.entanglement-garden-iam.svc.cluster.local:4434"},
		Services:          []string{"iam", "networking"},
		PluginCache:       "/tmp/webui-plugins",
		PluginSocketPath:  "/tmp/webui-plugin-sockets",
	}

	Version    = config.Version
	RootDomain = os.Getenv("EG_DOMAIN")
)

// Load loads the configuration off the disk
func Load() {
	if err := config.Load("entanglement.garden", "webui", &C); err != nil {
		logrus.Fatal("error loading configuration: ", err)
	}
}

// Kratos builds a new Kratos API client for the configured Kratos server
func (c Config) Kratos(admin bool) *kratos.APIClient {
	kratosConfig := kratos.NewConfiguration()
	if admin {
		kratosConfig.Servers = []kratos.ServerConfiguration{c.KratosAdminServer}
	} else {
		kratosConfig.Servers = []kratos.ServerConfiguration{c.KratosServer}
	}
	return kratos.NewAPIClient(kratosConfig)
}

// BaseImage is an image option to present in the UI when creating new VMs
type BaseImage struct {
	Name string `json:"name"`
	URL  string `json:"url"`
}
