#!/bin/bash
set -exuo pipefail
branch=$(git rev-parse --abbrev-ref HEAD)
image="codeberg.org/entanglementgarden/webui:${branch}"
make clean || true
make webui
podman build --build-arg "GOPROXY=${GOPROXY:-direct}" -t "${image}" -f - . <<EOF
FROM debian:stable
RUN apt-get update && apt-get install delve
ADD webui /entanglement-garden-webui
CMD ["/entanglement-garden-webui"]
EOF

podman push "${image}"
