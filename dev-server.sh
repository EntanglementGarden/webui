#!/bin/bash
set -exuo pipefail
make
podman build -t webui -f - . <<EOF
FROM debian:stable
ADD webui /webui
CMD ["/webui"]
EOF
podman stop webui || true # stop existing container if needed
podman run --rm --network entanglement-devtools --name webui  --env-file "$(pwd)/../devtools/entanglement-common.env" -v $(pwd)/../devtools/entanglement.garden:/etc/entanglement.garden webui
