// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package plugin_runner

import (
	"context"
	"net/http"
	"sync"

	echo "github.com/labstack/echo/v4"

	"entanglement.garden/api-client/egextensions"
	"entanglement.garden/common/httpx"
	"entanglement.garden/common/logging"
	"entanglement.garden/webui/config"
	"entanglement.garden/webui/static"
	"entanglement.garden/webui/templates"
)

var (
	cbServer   *echo.Echo
	webServer  *echo.Echo
	routerLock sync.Mutex

	notFoundPage = templates.Error{
		ErrTitle:    "404 Not Found",
		Description: "requested page was not found",
	}
)

func startServers() {
	ctx := context.TODO()
	log := logging.GetLog(ctx)

	cbServer = httpx.NewUnauthenticatedServer()
	cbServer.POST("/internal/extension-change", extensionChangeCallback)

	go func() {
		if err := cbServer.Start(":8080"); err != nil {
			logging.GetLog(ctx).Error("error starting callback webserver: ", err)
		}
	}()

	webServer = httpx.NewServer(errorHandlingMiddleware)
	webServer.GET("/", func(c echo.Context) error { return c.Redirect(http.StatusFound, "/entanglement.garden/rhyzome/") })
	webServer.StaticFS("/static", static.FS)
	webServer.RouteNotFound("/*", notFound)

	go func() {
		if err := webServer.Start(config.C.Bind); err != nil {
			log.Error("error starting server: ", err)
		}
	}()
}

func notFound(c echo.Context) error {
	writePageTemplate(c, &notFoundPage)
	return nil
}

func writePageTemplate(c echo.Context, p templates.Page) {
	templates.WritePageTemplate(c.Response().Writer, c.Request().Context(), c.Request().URL.Path, p)
}

func extensionChangeCallback(c echo.Context) error {
	var extension egextensions.WebhookPayload
	if err := c.Bind(&extension); err != nil {
		return err
	}

	go keepRunning(extension)

	return c.NoContent(http.StatusNoContent)
}
