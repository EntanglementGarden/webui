module entanglement.garden/webui

go 1.19

require (
	entanglement.garden/api-client v0.0.0-20231130195305-6be5db796d49
	entanglement.garden/common v0.0.0-20231208203011-089e7d4f2ba5
	entanglement.garden/webui-plugin v0.0.0-20231201191652-41a8cc57dc2b
	github.com/labstack/echo/v4 v4.11.1
	github.com/ory/kratos-client-go v1.0.0
	github.com/sirupsen/logrus v1.9.3
	github.com/valyala/quicktemplate v1.7.0
	google.golang.org/grpc v1.55.0
)

require (
	github.com/apapsch/go-jsonmerge/v2 v2.0.0 // indirect
	github.com/deepmap/oapi-codegen v1.12.4 // indirect
	github.com/golang-jwt/jwt/v4 v4.4.2 // indirect
	github.com/golang/protobuf v1.5.3 // indirect
	github.com/google/uuid v1.3.1 // indirect
	github.com/labstack/gommon v0.4.0 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.19 // indirect
	github.com/ory/keto/proto v0.11.1-alpha.0 // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	github.com/valyala/fasttemplate v1.2.2 // indirect
	golang.org/x/crypto v0.13.0 // indirect
	golang.org/x/net v0.14.0 // indirect
	golang.org/x/oauth2 v0.11.0 // indirect
	golang.org/x/sys v0.13.0 // indirect
	golang.org/x/text v0.13.0 // indirect
	google.golang.org/appengine v1.6.7 // indirect
	google.golang.org/genproto v0.0.0-20230306155012-7f2fa6fef1f4 // indirect
	google.golang.org/protobuf v1.31.0 // indirect
)
