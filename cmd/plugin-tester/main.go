package main

import (
	"context"
	"encoding/json"
	"net"
	"os"
	"time"

	"github.com/sirupsen/logrus"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"

	"entanglement.garden/webui-plugin/plugin_protos"
)

var (
	credentials = insecure.NewCredentials() // No SSL/TLS
	dialer      = func(ctx context.Context, addr string) (net.Conn, error) {
		var d net.Dialer
		return d.DialContext(ctx, "unix", addr)
	}
	options = []grpc.DialOption{
		grpc.WithTransportCredentials(credentials),
		grpc.WithBlock(),
		grpc.WithContextDialer(dialer),
	}
)

func main() {
	socketFile := os.Args[1]

	var conn *grpc.ClientConn
	var err error
	for i := 0; i < 5; i++ {
		conn, err = grpc.Dial(socketFile, options...)
		if err != nil {
			logrus.Warn("error connecting to plugin's socket: ", err)
			time.Sleep(time.Second)
			continue
		}

		break
	}
	if err != nil {
		logrus.Fatal(err)
	}

	client := plugin_protos.NewPluginServiceClient(conn)

	var settings *plugin_protos.Plugin
	for i := 0; i < 5; i++ {
		settings, err = client.Settings(context.Background(), &plugin_protos.Empty{})
		if err != nil {
			logrus.Warn("error reading plugin's settings: ", err)
			time.Sleep(time.Second)
			continue
		}
		break
	}
	if err != nil {
		logrus.Fatal(err)
	}

	if err := json.NewEncoder(os.Stdout).Encode(settings); err != nil {
		logrus.Warnf("settings that could not be encoded to json: %v", settings)
		logrus.Fatal("error encoding plugin settings to json: ", err)
	}
}
