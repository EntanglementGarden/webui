// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package plugin_runner

import (
	"context"
	"errors"
	"fmt"
	"io"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"sync"
	"time"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"

	"entanglement.garden/api-client/egapi"
	"entanglement.garden/api-client/egextensions"
	"entanglement.garden/common/httpx"
	"entanglement.garden/common/logging"
	"entanglement.garden/common/retry"
	"entanglement.garden/webui-plugin/plugin_protos"
	"entanglement.garden/webui/config"
	"entanglement.garden/webui/templates"
)

var (
	pluginTemp  = "/tmp"
	plugins     = map[string]runner{}
	pluginsLock sync.Mutex
)

// Start brings up the webservers, then downloads plugins from all configured extensions and runs them
func Start() {
	ctx, log := logging.LogWithField(context.Background(), "component", "plugin_runner")

	startServers()

	var err error
	pluginTemp, err = os.MkdirTemp("", "entanglement-webui")
	if err != nil {
		log.Fatal("error creating tempdir in ", config.C.PluginCache, ": ", err)
	}

	eg, err := egapi.NewInternalClient()
	if err != nil {
		log.Fatal("error initializing internal API client: ", err)
	}

	extensions, err := egextensions.NewClient(eg)
	if err != nil {
		log.Fatal("error creating extensions API client: ", err)
	}

	var currentExtensions *[]egextensions.WebhookPayload
	for currentExtensions == nil {
		log.Info("registering for extension change notifications")
		resp, err := extensions.WebhookRegisterWithResponse(ctx, "http://webui.entanglement-garden-webui.svc.cluster.local:8080/internal/extension-change")
		if err != nil {
			log.WithField("err", err).Debug("error registering webhook with extensions service, will retry")
			time.Sleep(time.Second * 30)
			continue
		}

		if resp.JSON200 == nil {
			log.WithField("status", resp.Status()).Debug("unexpected http response while registering webhook with extensions service, will retry")
			time.Sleep(time.Second * 30)
			continue
		}

		currentExtensions = resp.JSON200
	}

	for _, extension := range *currentExtensions {
		go keepRunning(extension)
	}
}

func Shutdown(ctx context.Context) {
	log := logging.GetLog(ctx)

	log.Info("shutting down callback server")
	if err := cbServer.Shutdown(ctx); err != nil {
		log.WithField("error", err).Error("error shutting down callback server")
	}

	log.Info("shutting down web server")
	if err := webServer.Shutdown(ctx); err != nil {
		log.WithField("error", err).Error("error shutting down web server")
	}

	pluginsLock.Lock()
	defer pluginsLock.Unlock()
	log.Info("shutting down all running plugins")
	for extension, runner := range plugins {
		l := log.WithField("extension", extension)

		l.Debug("closing grpc connection")
		if err := runner.conn.Close(); err != nil {
			l.Warn("error shutting down grpc connection to plugin: ", err)
		}

		l.Debug("killing process")
		if err := runner.cmd.Process.Kill(); err != nil {
			l.Warn("error killing process: ", err)
		}
	}

	log.Debug("cleaning up plugin temp directory")
	if err := os.RemoveAll(pluginTemp); err != nil {
		log.Error("error deleting plugin temp dir ", pluginTemp, ": ", err)
	}
}

func downloadPlugin(ctx context.Context, extension egextensions.WebhookPayload, filename string) error {
	endpoint := *extension.RestEndpoint
	log := logging.GetLog(ctx).WithField("endpoint", endpoint)

	u := fmt.Sprintf("%s/.well-known/entanglement.garden/console.ewp", endpoint) // ewp = Entanglement Web Plugin
	log.WithField("url", u).Debug("Downloading plugin")
	resp, err := httpx.GetHTTPClient(ctx).Get(u)
	if err != nil {
		return fmt.Errorf("failed to download plugin from %s: %v", u, err)
	}
	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		return fmt.Errorf("unexpected response while downloading %s: %d %s", u, resp.StatusCode, resp.Status)
	}

	f, err := os.Create(filename)
	if err != nil {
		return err
	}
	defer f.Close()

	if err := f.Chmod(0700); err != nil {
		return err
	}

	_, err = io.Copy(f, resp.Body)
	if err != nil {
		return err
	}

	return nil
}

func keepRunning(extension egextensions.WebhookPayload) {
	ctx, log := logging.LogWithField(context.Background(), "extension", extension.Extension)

	if extension.RestEndpoint == nil {
		log.Debugf("extension has no REST endpoint, ignoring: %+v", extension)
		return
	}

	dir := filepath.Join(pluginTemp, strings.ReplaceAll(extension.Extension, "/", "-"))
	err := os.Mkdir(dir, 0700)
	if err != nil && !errors.Is(err, os.ErrExist) {
		log.Fatal("error creating dir ", dir, ": ", err)
	}

	pluginFilename := fmt.Sprintf("%s-%s", strings.ReplaceAll(extension.Extension, "/", "-"), extension.Version)
	pluginFile := filepath.Join(dir, pluginFilename+".ewp")
	socketFile := filepath.Join(dir, pluginFilename+".sock")

	retryer := retry.New()
	for {
		if err := run(ctx, extension, pluginFile, socketFile); err != nil {
			retryer.Retry(ctx, err)
		}
	}
}

func run(ctx context.Context, extension egextensions.WebhookPayload, pluginFile, socketFile string) error {
	log := logging.GetLog(ctx)

	if err := downloadPlugin(ctx, extension, pluginFile); err != nil {
		log.Warn("error downloading plugin")
		return err
	}

	cmd := exec.Command(pluginFile, socketFile)
	cmd.Stdout = os.Stderr
	cmd.Stderr = os.Stderr
	if err := cmd.Start(); err != nil {
		log.Warn("error running plugin ", pluginFile)
		return err
	}

	success := false
	defer func() {
		if !success {
			if cmd.Process != nil {
				log.Debug("killing plugin")
				if err := cmd.Process.Kill(); err != nil {
					log.Error("error killing plugin: ", err)
				}
			}
		}
	}()

	var conn *grpc.ClientConn
	var err error
	for i := 0; i < 5; i++ {
		conn, err = grpc.Dial(fmt.Sprintf("unix:%s", socketFile), grpc.WithTransportCredentials(insecure.NewCredentials()))
		if err != nil {
			log.Warn("error connecting to plugin's socket: ", err)
			time.Sleep(time.Second)
			continue
		}

		break
	}
	if err != nil {
		return err
	}

	client := plugin_protos.NewPluginServiceClient(conn)

	var settings *plugin_protos.Plugin
	for i := 0; i < 5; i++ {
		settings, err = client.Settings(context.Background(), &plugin_protos.Empty{})
		if err != nil {
			if os.IsNotExist(err) {
				log.Warn("error reading plugin's settings: ", err)
			}
			time.Sleep(time.Second)
			continue
		}
		break
	}
	if err != nil {
		return err
	}

	r := runner{
		extensionID: extension.Extension,
		conn:        conn,
		client:      client,
		cmd:         cmd,
		settings:    settings,
	}

	addPlugin(extension.Extension, r)

	for _, pluginRoute := range settings.Paths {
		addRoute(ctx, pluginRoute, settings, r)
	}

	for _, menu := range settings.Menus {
		if menu == nil {
			continue
		}
		templates.AddPluginMenus(convertMenu(settings.Extension, menu))
	}

	success = true

	log.Info("loaded plugin")

	return cmd.Wait()
}

func addRoute(ctx context.Context, pluginRoute string, settings *plugin_protos.Plugin, r runner) {
	log := logging.GetLog(ctx)

	routerLock.Lock()
	defer routerLock.Unlock()

	// actualRoute is the path prefixed with the plugin's slug
	actualRoute := fmt.Sprintf("/%s%s", settings.Extension, pluginRoute)
	log.Debug("adding route ", actualRoute)
	webServer.Any(actualRoute, r.getHandler(actualRoute, pluginRoute))
	if pluginRoute == "/" {
		actualRoute = fmt.Sprintf("/%s", settings.Extension)
		log.Debug("adding route ", actualRoute)
		webServer.Any(actualRoute, r.getHandler(actualRoute, pluginRoute))
	}
}

func convertMenu(extension string, m *plugin_protos.MenuItem) templates.Menu {
	items := []templates.Menu{}
	for _, item := range m.Subitems {
		items = append(items, convertMenu(extension, item))
	}
	return templates.Menu{
		Name:  m.Name,
		Path:  fmt.Sprintf("/%s%s", extension, m.Path),
		Items: items,
	}
}

func addPlugin(extension string, r runner) {
	pluginsLock.Lock()
	defer pluginsLock.Unlock()

	plugins[extension] = r
}
