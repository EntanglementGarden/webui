// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package plugin_runner

import (
	"bytes"
	"fmt"
	"io"
	"os/exec"

	"entanglement.garden/common/httpx"
	"entanglement.garden/common/logging"
	"entanglement.garden/webui-plugin/plugin_protos"
	"entanglement.garden/webui/templates"
	echo "github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"
	"google.golang.org/grpc"
)

type runner struct {
	extensionID string

	conn     *grpc.ClientConn
	client   plugin_protos.PluginServiceClient
	cmd      *exec.Cmd
	settings *plugin_protos.Plugin
}

func (r runner) getHandler(actualRoute, pluginRoute string) echo.HandlerFunc {
	return func(c echo.Context) error {
		ctx := c.Request().Context()
		log := logging.GetLog(ctx)

		pathParams := map[string]string{}
		for _, param := range c.ParamNames() {
			pathParams[param] = c.Param(param)
		}

		if actualRoute == "/entanglement.garden/iam/user-settings" {
			log.Debug("user settings page: sending cookie value to plugin for this request")
			pathParams["cookie"] = c.Request().Header.Get("Cookie")
		}

		authToken, err := httpx.GetJWT(ctx)
		if err != nil {
			return fmt.Errorf("error getting JWT from context: %v", err)
		}

		log = log.WithFields(logrus.Fields{"route": actualRoute, "method": c.Request().Method, "plugin_route": pluginRoute})
		log.Debug("passing request to plugin")

		var body bytes.Buffer
		if _, err := io.Copy(&body, c.Request().Body); err != nil {
			return err
		}

		req := plugin_protos.Request{
			Route:      pluginRoute,
			Method:     c.Request().Method,
			Path:       c.Request().URL.String(),
			PathParams: pathParams,
			Body:       body.String(),
			AuthToken:  authToken.Raw,
		}

		resp, err := r.client.HandleRequest(ctx, &req)
		if err != nil {
			log.Error("error from plugin for ", r.settings.Extension, " while requesting ", actualRoute, ": ", err)
			return err
		}

		for k, v := range resp.Headers {
			c.Response().Header().Add(k, v)
		}

		if resp.StatusCode == 0 {
			resp.StatusCode = 200
		}

		if resp.Raw {
			return c.String(int(resp.StatusCode), resp.Body)
		}

		templates.WritePageTemplate(c.Response(), ctx, actualRoute, &templates.PluginPage{
			PageTitle: resp.Title,
			PageBody:  resp.Body,
		})

		return nil
	}
}
