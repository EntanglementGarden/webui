# Entanglement Garden Web UI
*a web GUI for Entanglement Garden end users*

this is an initial implementation of a web UI for Entanglement Garden. As with the rest of Entanglement Garden, it is very new and missing major features.

# development notes

before you start, copy `webui.example.json` to `webui.json`.

You will need to run a [Kratos](https://www.ory.sh/kratos/) server. To run in podman, create a directory `kratos-data` and run:

```
podman run --name ory-kratos --rm -v $(pwd)/kratos-dev:/etc/config/kratos:ro -v $(pwd)/kratos-data:/var/ory/kratos -p 4433:4433 -p 4434:4434 docker.io/oryd/kratos serve -c /etc/config/kratos/kratos.yml --dev --watch-courier
```

## rebuilding templates

To rebuild templates you must have [quicktemplate](https://github.com/valyala/quicktemplate) (`qtc`) installed:
```
go get -u github.com/valyala/quicktemplate/qtc
```

rebuild with `make` or `qtc -dir=templates`
