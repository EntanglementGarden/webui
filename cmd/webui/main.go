// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package main

import (
	"context"
	"os"
	"os/signal"
	"syscall"

	"github.com/sirupsen/logrus"

	"entanglement.garden/webui/config"
	"entanglement.garden/webui/plugin_runner"
)

var signals = make(chan os.Signal, 1)

func main() {
	signal.Notify(signals, syscall.SIGINT, syscall.SIGHUP)
	config.Load()

	go plugin_runner.Start()
	for {
		signal := <-signals
		logrus.Debug("Received signal: ", signal)
		switch signal {
		case syscall.SIGHUP:
			config.Load()
		case syscall.SIGINT:
			plugin_runner.Shutdown(context.TODO())
			return
		}
	}
}
