Copyright Entanglement Garden Developers
SPDX-License-Identifier: AGPL-3.0-only

{% import (
    "context"

    "entanglement.garden/webui/config"
) %}

{% interface
Page {
	Title()
	Body()
}
%}
{% func PageTemplate(ctx context.Context, route string, p Page) %}
<!DOCTYPE html>
<html lang="en">
<head>
    <title>{%= p.Title() %} | Entanglement Garden</title>
    <link rel="stylesheet" href="/static/style.css" />
</head>
<body>
<div class="wrapper">
    <div class="container header">
        <span class="brand">
            <p class="brand-name">Entanglement Garden</p>
            <p class="brand-domain">{%s config.RootDomain %}</p>
        </span>
        {%= RenderMenu(ctx, route) %}
    </div>
    <div class="container content">{%= p.Body() %}</div>
    <div class="container footer"><center>entanglement.garden/webui {%s config.Version %}</center></div>
</div>
</body>
</html>
{% endfunc %}

{% code type BasePage struct {} %}
{% func (p *BasePage) Title() %}This is a base title{% endfunc %}
{% func (p *BasePage) Body() %}This is a base body{% endfunc %}
