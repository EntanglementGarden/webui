# Entanglement Web Plugins

The Entanglement Web UI allows users to interact with any Entanglement component. To allow components to dictate how their UI should look,
each component may provide a plugin for the web UI, in the form of an Entanglement Web Plugin.

**At time of writing, the EWP interface is still a work in progress and subject to change**

Entanglement Web Plugins (EWPs) take the form of executable binary files. They are served from the component's HTTP server at path
`/.well-known/entanglement.garden/<service>.ewp`. When executed, they should start a grpc listener on a unix socket which can answer
requests for HTTP requests with pages that look like the rest of the Entanglement Web UI. While documentation is currently limited,
the Entanglement IAM service contains a sample EWP in the `web/` directory.
