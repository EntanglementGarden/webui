// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package utils

import (
	"context"

	kratos "github.com/ory/kratos-client-go"
)

type contextKey string

const (
	sessionContextKey   = contextKey("kratos-session")
	authTokenContextKey = contextKey("auth-token")
)

// GetSession returns the kratos session from the context
func GetSession(ctx context.Context) *kratos.Session {
	iface := ctx.Value(sessionContextKey)
	if iface == nil {
		return nil
	}
	return iface.(*kratos.Session)
}

// WithSession returns a context with a session attached
func WithSession(ctx context.Context, session *kratos.Session) context.Context {
	return context.WithValue(ctx, sessionContextKey, session)
}

// WithAuthToken retuns a conext with a cookie attached
func WithAuthToken(ctx context.Context, token string) context.Context {
	return context.WithValue(ctx, authTokenContextKey, token)
}
