// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package utils

import (
	"context"
	"errors"
	"net/http"

	"entanglement.garden/api-client/egapi"
)

// AddAuthToken adds the auth token from the context to an outgoing IAM request
func AddAuthToken(ctx context.Context, req *http.Request) error {
	tokenMaybe := ctx.Value(authTokenContextKey)
	if tokenMaybe == nil {
		return errors.New("no auth token found on context")
	}

	token, ok := tokenMaybe.(string)
	if !ok {
		return errors.New("token is not a string")
	}

	req.Header.Set("Authorization", token)

	return nil
}

func GetAPIClient(ctx context.Context) (*egapi.Client, error) {
	tokenMaybe := ctx.Value(authTokenContextKey)
	if tokenMaybe == nil {
		return nil, errors.New("no auth token found on context")
	}

	token, ok := tokenMaybe.(string)
	if !ok {
		return nil, errors.New("token is not a string")
	}

	client, err := egapi.NewClient(egapi.InternalAuth)
	if err != nil {
		return nil, err
	}

	client.InternalJWT = token

	return client, nil
}
