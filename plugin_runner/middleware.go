// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package plugin_runner

import (
	"fmt"

	echo "github.com/labstack/echo/v4"

	"entanglement.garden/webui/templates"
)

func errorHandlingMiddleware(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		defer crashHandler(c)

		err := next(c)
		if err != nil {
			writePageTemplate(c, &templates.Error{Error: err})
		}

		return nil
	}
}

func crashHandler(c echo.Context) {
	errAny := recover()
	if errAny == nil {
		return
	}

	if err, ok := errAny.(error); ok {
		writePageTemplate(c, &templates.Error{Error: err})
	} else {
		writePageTemplate(c, &templates.Error{
			ErrTitle:    "unexpected error",
			Description: fmt.Sprintf("%+v", errAny),
		})
	}
}
