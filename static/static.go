// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package static

import (
	"embed"
)

var (
	//go:embed *
	FS embed.FS
)
