webui: templates $(shell find -name '*.go' -print) static/*
	go build -trimpath -ldflags "-X entanglement.garden/common/config.Version=$(shell git describe --always --tags)" ./cmd/webui

templates: templates/*/*.qtpl templates/*.qtpl
	qtc -dir=templates

clean:
	-rm webui
